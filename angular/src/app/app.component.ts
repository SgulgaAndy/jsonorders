import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Orders} from './orders';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  orders: Orders;

  config: any;
  collection = { count: 100, data: [] };

  constructor(private http: HttpClient) {

    for (let i = 0; i < this.collection.count; i++) {
      this.collection.data.push(
        {
          id: i + 1,
          value: 'items number ' + (i + 1)
        }
      );
    }

    this.config = {
      itemsPerPage: 10,
      currentPage: 1,
      totalItems: this.collection.count
    };
  }

  ngOnInit(){
    this.http.get('http://127.0.0.1:8000/api/orders').subscribe((data: Orders) => this.orders = data);
  }
  sendCancelRequest(id){
    // console.log('hello');
    this.http.post('http://127.0.0.1:8000/api/update/' + id, {}).subscribe((data) => {
      this.ngOnInit();
    });
  }

  pageChanged(event){
    this.config.currentPage = event;
  }

}
