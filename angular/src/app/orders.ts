export class Orders{
  id: number;
  date: string;
  customer: string;
  address1: string;
  city: string;
  postcode: string;
  country: string;
  amount: string;
  status: string;
  deleted: string;
  last_modified: string;
}
