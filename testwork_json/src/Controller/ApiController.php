<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    /**
     * @Route("/api/orders", name="api")
     */
    public function index()
    {

        return $this->render('api/index.html.twig', [
            'controller_name' => 'ApiController',
        ]);
    }

    /**
     * @Route("/api/update/{id}", name="update")
     */
    public function update($id)
    {

        $jsonOrders = file_get_contents('/var/www/testwork_json/templates/api/index.html.twig');
        $orders = json_decode($jsonOrders);

        foreach($orders as $order)
        {
            if($order->id == $id)
            {
                $order->status = 'cancelled';
            }
        }
        file_put_contents('/var/www/testwork_json/templates/api/index.html.twig', json_encode($orders));

        return new JsonResponse(['data' => true]);
    }
}
